#!/usr/bin/python3

import os, time, threading, sys
from urllib.request import Request, urlopen

from base64 import b64encode

API_KEY = input('Insira seu [TinyPNG] API ID: ')

if not API_KEY:
	# Você pode definir uma chave padrão aqui
	# Assim não é necessário ficar
	# informando a chave sempre
	# que executar o comando
	API_KEY = ""

OVERWRITE_FILES = input('Deseja que as imagens originais sejam sobrescritas com as imagens compiladas?[y/n] ')
if OVERWRITE_FILES[0] == 'y':
	TINY_PREFIX = ''
else:
	TINY_PREFIX = 'tiny-'

RECURSIVELY = input('Deseja executar o comando recursivamente nas pastas?[y/n] ')
RECURSIVELY = RECURSIVELY[0] == 'y'

DEFAULT_PATH = os.environ['HOME'] + '~/tinyPNG/'
PATH_TO_SHRINK = sys.argv[1] if(len(sys.argv) > 1) else DEFAULT_PATH
IGNORED_FILES = '.DS_Store'

def get_file_names(path = PATH_TO_SHRINK):
	list = []
	for file in os.listdir(path):
		if os.path.isdir(path+file) and RECURSIVELY and not file in IGNORED_FILES:
			list.extend(get_file_names(path+file+'/'))

		elif is_image(path+file):
			list.append(path+file)

	return list

def is_image(filename):
	result = os.path.isfile(filename)
	result = result and (not filename in IGNORED_FILES)
	result = result and (filename.split('/')[-1].split('-')[0] != 'tiny')
	return result and (filename.split('.')[-1] == 'png')

def shrink_image(image):
	request = Request("https://api.tinypng.com/shrink", open(image, "rb").read())

	auth = b64encode(bytes("api:" + API_KEY, "ascii")).decode("ascii")
	request.add_header("Authorization", "Basic %s" % auth)

	response = urlopen(request)
	if response.status == 201:
	  # Compression was successful, retrieve output from Location header.
	  result = urlopen(response.getheader("Location")).read()

	  new_filename = TINY_PREFIX+image.split('/')[-1]
	  image_path = image.split('/')[:-1]
	  new_filename = '/'.join(image_path) + '/' + new_filename

	  open(new_filename, "wb").write(result)
	  print("Compression successful:", new_filename)

	else:
	  # Something went wrong! You can parse the JSON body for details.
	  print("Compression failed:", image)

class Shrink(threading.Thread):
	def __init__(self, filename):
		threading.Thread.__init__(self)
		self.stop = False
		self.filename = filename

	def run(self):
		print("Shrink:", self.filename)
		shrink_image(self.filename)

for file in get_file_names():
	shrink = Shrink(file)
	shrink.start()