# Comprimindo imagens com o TinyPNG

Este script foi escrito em python3 por isso é preciso te-lo instalado.

---
	
###Passo a passo:
**Baixe o projeto pelo git**  
```console
    git clone https://gitlab.com/nvieirarafael/TinyPNGProcessor.git
```  
_Ou baixe o [arquivo](https://gitlab.com/nvieirarafael/TinyPNGProcessor/repository/archive.zip) zip._

**Execute o processador:**  
```console
    ./process.py
```

Por padrão ele ir processar o tinyPNG na pasta _~/tinyPNG_ em sua maquina.  
**Caso você queira executa-lo em outra pasta** basta executar o comando com um parametro para o local desejado:
```console
    ./process.py ~/local/onde/processar/imagens
```

Logo que executado ele ira pedir o ID da API do TinyPNG que você pode conseguir efetuando um cadastro no [site](https://tinypng.com/developers)  

Dentro do arquivo process.py você pode definir uma chave padrão, assim não preciso informa-la sempre que executar o comando:
```python
if not API_KEY:
    # Você pode definir uma chave padrão aqui
    # Assim não é necessário ficar
    # informando a chave sempre
    # que executar o comando
    API_KEY = "Insira sua chave da API aqui"
```

	
Finalmente se tudo der certo você vera as mensagens:

```console
    Shrink: /Users/Rafael/Desktop/tiny_png/9996_i_0_0_background.png
    Shrink: /Users/Rafael/Desktop/tiny_png/9997_i_490_36_personagens.png
	Compression successful: /Users/Rafael/Desktop/tiny_png/tiny-9999_i_1036_114_zzz.png
    Compression successful: /Users/Rafael/Desktop/tiny_png/tiny-9998_i_31_211_texto.png
```

Todos os arquivos processados iram ter o prefixo 'tiny-' no nome.

_Uma conta FREE da API do tinyPNG permite executar até 500 imagens por mês._